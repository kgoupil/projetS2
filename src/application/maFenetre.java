package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.*;
import classes.*;

public class maFenetre extends Stage {
	private final int lC=60; 
	private Plateau plateau;
    
    public maFenetre() {    
        this.setTitle("Jeu de Dames");
        this.setX(100);
        this.setY(100);
        this.setResizable(false);
        Scene laScene = new Scene(creerContenu(),1200,696);
        this.setScene(laScene);
        this.sizeToScene();
    }
    Parent creerContenu() {
    	
    	Menu menu1 = new Menu("Configurer");
    	Menu subMenu = new Menu("PLateau");
    	MenuItem menuItem11 = new MenuItem("Noir/Blanc");
    	MenuItem menuItem12 = new MenuItem("Naturel");
    	subMenu.getItems().add(menuItem11);
    	subMenu.getItems().add(menuItem12);
    	menu1.getItems().add(subMenu);
    	
    	MenuItem menuItem1 = new MenuItem("NomJoueurs");
    	menu1.getItems().add(menuItem1);
    	
    	
    	Menu menu2 = new Menu("Nouvelle Partie");
    	Menu menu3 = new Menu("Pause");// Options : Couleur de plateau noire rouge
    	MenuBar menuBar = new MenuBar();
    	menuBar.getMenus().add(menu1);
    	menuBar.getMenus().add(menu2);
    	menuBar.getMenus().add(menu3);
    	
        BorderPane root=new BorderPane();
		plateau=new Plateau(lC);
        root.setStyle("-fx-background-color: #f0ffff; "); //724b15   #a5f5dc
        Circle c=new Circle(50,50,20);
        c.setFill(Color.BLUE);
        int tailleCase;
        tailleCase=60;
        
        
        
        
        int numerotour;
        numerotour=0;
        String JoueurEnCours;
        JoueurEnCours="<NomJoueurEnCours>";
        String Joueur1;
        Joueur1="<NomJ1>";
        String Joueur2;
        Joueur2="<NomJ2>";
        String MinJ1;
        MinJ1="<Min>";
        String SecJ1;
        SecJ1="<Sec>";
        
        String MinJ2;
        MinJ2="<Min>";
        String SecJ2;
        SecJ2="<Sec>";
        Button button1 = new Button("Abandonner");
        Label labeltour = new Label("   Tour n�"+numerotour+"                        "+"<Min>"+":"+"<Sec>"+"                   "+JoueurEnCours);
        Label label1 = new Label(Joueur1);
        Label label2 = new Label(MinJ1+" : "+SecJ1);
        Label label3 = new Label(Joueur2);
        Label label4 = new Label(MinJ2+" : "+SecJ2);
        button1.setMinSize(tailleCase*3+20, 40);
        button1.setMaxSize(tailleCase*3+20, 40);
        labeltour.setMinSize(tailleCase*10,50);
        labeltour.setMaxSize(tailleCase*10,50);
        labeltour.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        label1.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        label2.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        label3.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        label4.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        HBox hboxtop = new HBox(labeltour);
        hboxtop.setPrefSize(300, 50);
        labeltour.setPadding(new Insets(15,0,10,0)); //top, right, bottom, left
        labeltour.setTextAlignment(TextAlignment.CENTER);
        hboxtop.setPadding(new Insets(10,0,10,300));
        label1.setPadding(new Insets(0,0,0,60)); 
        label2.setPadding(new Insets(0,0,0,40)); 
        label3.setPadding(new Insets(0,0,0,60)); 
        label4.setPadding(new Insets(10,0,0,40)); 
        label1.setMinSize(200,40);
        label1.setMaxSize(200,40);
        label2.setMinSize(200,40);
        label2.setMaxSize(200,40);
        label3.setMinSize(200,40);
        label3.setMaxSize(200,40);
        label4.setMinSize(200,40);
        label4.setMaxSize(200,40);
        button1.setPadding(new Insets(0,0,0,20));

        Alert a = new Alert(AlertType.NONE);
        
        EventHandler<ActionEvent> event = new EventHandler<ActionEvent>() {

            public void handle(ActionEvent e)
            {
                a.setAlertType(AlertType.CONFIRMATION);
                a.setContentText("�tes-vous s�r de vouloir abandonner?");
                a.show();
            	
            }
        };
        button1.setOnAction(event);
        
        labeltour.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, null , null)));
        label1.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, null , null)));
        label2.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, null , null)));
        label3.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, null , null)));
        label4.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, null , null)));
    	
        
        
        
        
        VBox vboxright = new VBox(label3, label4,  button1);
        VBox vboxleft = new VBox(label1, label2);

        
        vboxright.setPadding(new Insets(0,60,0,0));
        vboxright.setSpacing(20);
        vboxleft.setPadding(new Insets(0,0,0,60));
        vboxleft.setSpacing(20);
        root.setCenter(plateau);
        root.setLeft(vboxleft);
        root.setRight(vboxright);
        root.setTop(hboxtop);
        BorderPane.setAlignment(plateau, Pos.CENTER);
        BorderPane.setAlignment(vboxleft, Pos.CENTER);
        BorderPane.setAlignment(vboxright, Pos.CENTER);
        BorderPane.setAlignment(hboxtop, Pos.CENTER);
        BorderPane.setAlignment(vboxleft, Pos.CENTER);
        BorderPane.setAlignment(vboxright, Pos.CENTER);
        VBox vBoxTotal = new VBox(menuBar, root);
        return vBoxTotal;
    }
}