package classes;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

public class Piece extends Circle {
	private char couleur;
	private boolean dame;
	private boolean estUneDame;
	private boolean deplacementUniquement;
	private boolean selectionnee;
	private Case maCase;
	private Plateau plateau;
	private Image dameN=new Image("./dameN.png");
	private Image dameB=new Image("./dameB.png");
	public Piece(double lC,Case c) {
		super();
		setDame(false);
		setEstUneDame(false);
		setDeplacementUniquement(false);
		selectionnee=false;
		maCase=c;
		this.plateau=maCase.getPlateau();
		this.setRadius(lC*0.42);
		this.setCenterX((c.getX()+0.5)*lC);
		this.setCenterY((c.getY()+0.5)*lC);
		if(maCase.getY()<=3) {
			couleur='n';
			this.setFill(Color.BLACK);
		}
		else if(maCase.getY()>=6) {
			couleur='b';
			this.setFill(Color.WHITE);
		}
		this.setOnMouseClicked(e -> selectionner());
	}
	public void selectionner() {
		if(selectionnee==true) {
			selectionnee=false;
			plateau.deselectionnerTout();
			if(couleur=='b') {
				if(estUneDame==false) {
					this.setFill(Color.WHITE);
				}
				else {
					this.setFill(new ImagePattern(dameB));
				}
			}
			else if(couleur=='n') {
				if(estUneDame==false) {
					this.setFill(Color.BLACK);
				}
				else {
					this.setFill(new ImagePattern(dameN));
				}
			}
		}
		else if(selectionnee==false) {
			if(couleur=='b'&&plateau.uneBlancheSelectionnee()==false&&plateau.getTour()==1) {
				selectionnee=true;
				plateau.setPieceSelectionnee(this);
				this.setFill(Color.LIGHTBLUE);
				if(dame==false) {
					if(maCase.getX()-1>=0&&maCase.getY()-1>=0) {
						if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()-1).getPiece()==null) {
							setDeplacementUniquement(true);
							plateau.getCaseNoire(maCase.getX()-1,maCase.getY()-1).selection();
						}
						else if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()-1).getPiece().getCouleur()=='n') {
							if(maCase.getX()-2>=0&&maCase.getY()-2>=0) {
								if(plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).getPiece()==null) {
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).selection();
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).setManger(true);
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).setmX(-1);
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).setmY(-1);
								}
							}
						}
					}
					if(maCase.getX()+1<=9&&maCase.getY()-1>=0) {
						if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()-1).getPiece()==null) {
							setDeplacementUniquement(true);
							plateau.getCaseNoire(maCase.getX()+1,maCase.getY()-1).selection();
						}
						else if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()-1).getPiece().getCouleur()=='n') {
							if(maCase.getX()+2<=9&&maCase.getY()-2>=0) {
								if(plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).getPiece()==null) {
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).selection();
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).setManger(true);
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).setmX(1);
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).setmY(-1);
								}
							}
						}
					}
					if(maCase.getX()-1>=0&&maCase.getY()+1<=9) {
						if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()+1).getPiece()!=null) {
							if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()+1).getPiece().getCouleur()=='n') {
								if(maCase.getX()-2>=0&&maCase.getY()+2<=9) {
									if(plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).getPiece()==null) {
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).selection();
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).setManger(true);
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).setmX(-1);
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).setmY(1);
									}
								}
							}
						}
					}
					if(maCase.getX()+1<=9&&maCase.getY()+1<=9) {
						if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()+1).getPiece()!=null) {
							if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()+1).getPiece().getCouleur()=='n') {
								if(maCase.getX()+2<=9&&maCase.getY()+2<=9) {
									if(plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).getPiece()==null) {
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).selection();
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).setManger(true);
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).setmX(1);
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).setmY(1);
									}
								}
							}
						}
					}
				}
				else {
					boolean stop1=false;
					boolean stop2=false;
					boolean stop3=false;
					boolean stop4=false;
					int ajout=1;
					while((stop1==false||stop2==false||stop3==false||stop4==false)&&ajout<10) {
						if(stop1==false) {
							if(maCase.getX()+ajout<=9&&maCase.getY()+ajout<=9) {
								if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()+ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()+ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()+ajout).getPiece().getCouleur()=='n') {
									if(maCase.getX()+(ajout+1)<=9&&maCase.getY()+(ajout+1)<=9) {
										if(plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).setmX(1);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).setmY(1);
											stop1=true;
										}else {stop1=true;}}else {stop1=true;}}else {stop1=true;}}else {stop1=true;}
						}
						if(stop2==false) {
							if(maCase.getX()-ajout>=0&&maCase.getY()+ajout<=9) {
								if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()+ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()+ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()+ajout).getPiece().getCouleur()=='n') {
									if(maCase.getX()-(ajout+1)>=0&&maCase.getY()+(ajout+1)<=9) {
										if(plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).setmX(-1);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).setmY(1);
											stop2=true;
										}else {stop2=true;}}else {stop2=true;}}else {stop2=true;}}else {stop2=true;}
						}
						if(stop3==false) {
							if(maCase.getX()+ajout<=9&&maCase.getY()-ajout>=0) {
								if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()-ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()-ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()-ajout).getPiece().getCouleur()=='n') {
									if(maCase.getX()+(ajout+1)<=9&&maCase.getY()-(ajout+1)>=0) {
										if(plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).setmX(1);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).setmY(-1);
											stop3=true;
										}else {stop3=true;}}else {stop3=true;}}else {stop3=true;}}else {stop3=true;}
						}
						if(stop4==false) {
							if(maCase.getX()-ajout>=0&&maCase.getY()-ajout>=0) {
								if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()-ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()-ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()-ajout).getPiece().getCouleur()=='n') {
									if(maCase.getX()-(ajout+1)>=0&&maCase.getY()-(ajout+1)>=0) {
										if(plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).setmX(-1);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).setmY(-1);
											stop4=true;
										}else {stop4=true;}}else {stop4=true;}}else {stop4=true;}}else {stop4=true;}
						}
						ajout++;
					}
				}
			}
			else if(couleur=='n'&&plateau.uneNoireSelectionnee()==false&&plateau.getTour()==2) {
				selectionnee=true;
				plateau.setPieceSelectionnee(this);
				this.setFill(Color.LIGHTBLUE);
				if(dame==false) {
					if(maCase.getX()-1>=0&&maCase.getY()+1<=9) {
						if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()+1).getPiece()==null) {
							setDeplacementUniquement(true);
							plateau.getCaseNoire(maCase.getX()-1,maCase.getY()+1).selection();
						}
						else if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()+1).getPiece().getCouleur()=='b') {
							if(maCase.getX()-2>=0&&maCase.getY()+2<=9) {
								if(plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).getPiece()==null) {
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).selection();
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).setManger(true);
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).setmX(-1);
									plateau.getCaseNoire(maCase.getX()-2,maCase.getY()+2).setmY(1);
								}
							}
						}
					}
					if(maCase.getX()+1<=9&&maCase.getY()+1<=9) {
						if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()+1).getPiece()==null) {
							setDeplacementUniquement(true);
							plateau.getCaseNoire(maCase.getX()+1,maCase.getY()+1).selection();
						}
						else if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()+1).getPiece().getCouleur()=='b') {
							if(maCase.getX()+2<=9&&maCase.getY()+2<=9) {
								if(plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).getPiece()==null) {
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).selection();
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).setManger(true);
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).setmX(1);
									plateau.getCaseNoire(maCase.getX()+2,maCase.getY()+2).setmY(1);
								}
							}
						}
					}
					if(maCase.getX()+1<=9&&maCase.getY()-1>=0) {
						if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()-1).getPiece()!=null) {
							if(plateau.getCaseNoire(maCase.getX()+1,maCase.getY()-1).getPiece().getCouleur()=='b') {
								if(maCase.getX()+2<=9&&maCase.getY()-2>=0) {
									if(plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).getPiece()==null) {
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).selection();
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).setManger(true);
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).setmX(1);
										plateau.getCaseNoire(maCase.getX()+2,maCase.getY()-2).setmY(-1);
									}
								}
							}
						}
					}
					if(maCase.getX()-1>=0&&maCase.getY()-1>=0) {
						if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()-1).getPiece()!=null) {
							if(plateau.getCaseNoire(maCase.getX()-1,maCase.getY()-1).getPiece().getCouleur()=='b') {
								if(maCase.getX()-2>=0&&maCase.getY()-2>=0) {
									if(plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).getPiece()==null) {
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).selection();
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).setManger(true);
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).setmX(-1);
										plateau.getCaseNoire(maCase.getX()-2,maCase.getY()-2).setmY(-1);
									}
								}
							}
						}
					}
				}
				else {
					boolean stop1=false;
					boolean stop2=false;
					boolean stop3=false;
					boolean stop4=false;
					int ajout=1;
					while((stop1==false||stop2==false||stop3==false||stop4==false)&&ajout<10) {
						if(stop1==false) {
							if(maCase.getX()+ajout<=9&&maCase.getY()+ajout<=9) {
								if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()+ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()+ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()+ajout).getPiece().getCouleur()=='b') {
									if(maCase.getX()+(ajout+1)<=9&&maCase.getY()+(ajout+1)<=9) {
										if(plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).setmX(1);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()+(ajout+1)).setmY(1);
											stop1=true;
										}else {stop1=true;}}else {stop1=true;}}else {stop1=true;}}else {stop1=true;}
						}
						if(stop2==false) {
							if(maCase.getX()-ajout>=0&&maCase.getY()+ajout<=9) {
								if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()+ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()+ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()+ajout).getPiece().getCouleur()=='b') {
									if(maCase.getX()-(ajout+1)>=0&&maCase.getY()+(ajout+1)<=9) {
										if(plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).setmX(-1);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()+(ajout+1)).setmY(1);
											stop2=true;
										}else {stop2=true;}}else {stop2=true;}}else {stop2=true;}}else {stop2=true;}
						}
						if(stop3==false) {
							if(maCase.getX()+ajout<=9&&maCase.getY()-ajout>=0) {
								if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()-ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()-ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()+ajout,maCase.getY()-ajout).getPiece().getCouleur()=='b') {
									if(maCase.getX()+(ajout+1)<=9&&maCase.getY()-(ajout+1)>=0) {
										if(plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).setmX(1);
											plateau.getCaseNoire(maCase.getX()+(ajout+1),maCase.getY()-(ajout+1)).setmY(-1);
											stop3=true;
										}else {stop3=true;}}else {stop3=true;}}else {stop3=true;}}else {stop3=true;}
						}
						if(stop4==false) {
							if(maCase.getX()-ajout>=0&&maCase.getY()-ajout>=0) {
								if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()-ajout).getPiece()==null) {
									setDeplacementUniquement(true);
									plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()-ajout).selection();
								}
								else if(plateau.getCaseNoire(maCase.getX()-ajout,maCase.getY()-ajout).getPiece().getCouleur()=='b') {
									if(maCase.getX()-(ajout+1)>=0&&maCase.getY()-(ajout+1)>=0) {
										if(plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).getPiece()==null) {
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).selection();
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).setManger(true);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).setmX(-1);
											plateau.getCaseNoire(maCase.getX()-(ajout+1),maCase.getY()-(ajout+1)).setmY(-1);
											stop4=true;
										}else {stop4=true;}}else {stop4=true;}}else {stop4=true;}}else {stop4=true;}
						}
						ajout++;
					}
				}
			}
		}
	}
	public boolean isSelectionne() {
		return selectionnee;
	}
	public char getCouleur() {
		return couleur;
	}
	public Case getMaCase() {
		return maCase;
	}
	public void setMaCase(Case maCase) {
		this.maCase = maCase;
	}
	public void deplacement() {
		this.setCenterX((maCase.getX()+0.5)*60);
		this.setCenterY((maCase.getY()+0.5)*60);
	}
	public boolean isDame() {
		return dame;
	}
	public void setDame(boolean dame) {
		this.dame = dame;
	}
	public boolean isEstUneDame() {
		return estUneDame;
	}
	public void setEstUneDame(boolean estUneDame) {
		this.estUneDame = estUneDame;
	}
	public boolean isDeplacementUniquement() {
		return deplacementUniquement;
	}
	public void setDeplacementUniquement(boolean deplacementUniquement) {
		this.deplacementUniquement = deplacementUniquement;
	}
}
