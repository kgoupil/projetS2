package classes;

import javafx.scene.Group;
import javafx.scene.layout.GridPane;

public class Plateau extends Group {
	private Case[] casesNoires=new Case[50];
	private Piece[] pionsNoirs=new Piece[20];
	private Piece[] pionsBlancs=new Piece[20];
	private Piece pieceSelectionnee;
	private int tour;
	private int nbPieceB;
	private int nbPieceN;
	public Plateau(int lC) {
		super();
		setTour(1);
		setNbPieceB(20);
		setNbPieceN(20);
		GridPane grid=new GridPane();
		grid.setStyle("-fx-background-color: #f5f5dc; ");
		grid.setGridLinesVisible(false);
		this.getChildren().add(grid);
		int cN=0;
		int nPN=0;
		int nPB=0;
		for(int j=0;j<10;j++) {
			for(int i=0;i<10;i++) {
				if(((j%2==0)&&(i%2==1))||((j%2==1)&&(i%2==0))) {
					casesNoires[cN]=new Case(lC,i,j,this);
					grid.add(casesNoires[cN],i,j);
					if(j<=3) {
						pionsNoirs[nPN]=new Piece(lC,casesNoires[cN]);
						this.getChildren().add(pionsNoirs[nPN]);
						casesNoires[cN].setPiece(pionsNoirs[nPN]);
						nPN++;
					}
					else if(j>=6) {
						pionsBlancs[nPB]=new Piece(lC,casesNoires[cN]);
						this.getChildren().add(pionsBlancs[nPB]);
						casesNoires[cN].setPiece(pionsBlancs[nPB]);
						nPB++;
					}
					else {
						casesNoires[cN].setPiece(null);
					}
					cN++;
				}
			}
		}
	}
	public boolean uneBlancheSelectionnee() {
		boolean trouve=false;
		for(int i=0;i<20;i++) {
			if(pionsBlancs[i].isSelectionne()==true) {
				trouve=true;
			}
		}
		return trouve;
	}
	public boolean uneNoireSelectionnee() {
		boolean trouve=false;
		for(int i=0;i<20;i++) {
			if(pionsNoirs[i].isSelectionne()==true) {
				trouve=true;
			}
		}
		return trouve;
	}
	public Case getCaseNoire(int x,int y) {
		Case c=null;
		for(int i=0;i<50;i++) {
			if(casesNoires[i].getX()==x&&casesNoires[i].getY()==y) {
				c=casesNoires[i];
			}
		}
		return c;
	}
	public void deselectionnerTout() {
		for(int i=0;i<50;i++) {
			if(casesNoires[i].isSelectionnee()==true) {
				casesNoires[i].selection();
			}
		}
	}
	public Piece getPieceSelectionnee() {
		return pieceSelectionnee;
	}
	public void setPieceSelectionnee(Piece pieceSelectionnee) {
		this.pieceSelectionnee = pieceSelectionnee;
	}
	public void finManger() {
		for(int i=0;i<50;i++) {
			if(casesNoires[i].isManger()==true) {
				casesNoires[i].setManger(false);
			}
		}
	}
	public void remettreDame() {
		for(int i=0;i<20;i++) {
			pionsNoirs[i].setDeplacementUniquement(false);
			pionsBlancs[i].setDeplacementUniquement(false);
			if(tour==1) {
				if(pionsNoirs[i].isEstUneDame()==true) {
					pionsNoirs[i].setDame(true);
				}
			}
			else if(tour==2) {
				if(pionsBlancs[i].isEstUneDame()==true) {
					pionsBlancs[i].setDame(true);
				}
			}
		}
	}
	public void finDeTour() {
		if(nbPieceB==0) {
			System.out.println("Victoire des Noirs");
		}
		else if(nbPieceN==0) {
			System.out.println("Victoire des Blancs");
		}
		else {
			remettreDame();
			if(tour==1) {
				tour=2;
			}
			else if(tour==2) {
				tour=1;
			}
		}
	}
	public int getTour() {
		return tour;
	}
	public void setTour(int tour) {
		this.tour = tour;
	}
	public int getNbPieceB() {
		return nbPieceB;
	}
	public void setNbPieceB(int nbPieceB) {
		this.nbPieceB = nbPieceB;
	}
	public int getNbPieceN() {
		return nbPieceN;
	}
	public void setNbPieceN(int nbPieceN) {
		this.nbPieceN = nbPieceN;
	}
}