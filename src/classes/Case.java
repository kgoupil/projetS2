package classes;

import javafx.scene.control.Button;

public class Case extends Button {
	private int x;
	private int y;
	private int mX;
	private int mY;
	private boolean manger;
	private Piece piece;
	private Plateau plateau;
	private boolean selectionnee;
	public Case(int l,int x,int y,Plateau plateau) {
		super();
		this.setPrefSize(l,l);
		this.setMinSize(l, l);
		this.x=x;
		this.y=y;
		this.plateau=plateau;
		this.setPiece(null);
		selectionnee=false;
		this.setStyle("-fx-background-color: #724b15; ");
	}
	public void selection() {
		if(selectionnee==true) {
			selectionnee=false;
			this.setStyle("-fx-background-color: #724b15; ");
			this.setOnAction(e -> {});
		}
		else {
			selectionnee=true;
			this.setStyle("-fx-background-color: #add8e6; ");
			this.setOnAction(e -> deplacerPiece());
		}
	}
	public void manger() {
		if(mX==1&&mY==1) {
			if(plateau.getCaseNoire(x-1, y-1).getPiece()!=null) {
				if(plateau.getCaseNoire(x-1, y-1).getPiece().getCouleur()=='b') {
					plateau.setNbPieceB(plateau.getNbPieceB()-1);
				}
				else if(plateau.getCaseNoire(x-1, y-1).getPiece().getCouleur()=='n') {
					plateau.setNbPieceN(plateau.getNbPieceN()-1);
				}
				plateau.getCaseNoire(x-1, y-1).getPiece().setVisible(false);
				plateau.getCaseNoire(x-1, y-1).getPiece().setMaCase(null);
				plateau.getCaseNoire(x-1, y-1).setPiece(null);
			}
		}
		else if(mX==-1&&mY==1) {
			if(plateau.getCaseNoire(x+1, y-1).getPiece()!=null) {
				if(plateau.getCaseNoire(x+1, y-1).getPiece().getCouleur()=='b') {
					plateau.setNbPieceB(plateau.getNbPieceB()-1);
				}
				else if(plateau.getCaseNoire(x+1, y-1).getPiece().getCouleur()=='n') {
					plateau.setNbPieceN(plateau.getNbPieceN()-1);
				}
				plateau.getCaseNoire(x+1, y-1).getPiece().setVisible(false);
				plateau.getCaseNoire(x+1, y-1).getPiece().setMaCase(null);
				plateau.getCaseNoire(x+1, y-1).setPiece(null);
			}
		}
		else if(mX==1&&mY==-1) {
			if(plateau.getCaseNoire(x-1, y+1).getPiece()!=null) {
				if(plateau.getCaseNoire(x-1, y+1).getPiece().getCouleur()=='b') {
					plateau.setNbPieceB(plateau.getNbPieceB()-1);
				}
				else if(plateau.getCaseNoire(x-1, y+1).getPiece().getCouleur()=='n') {
					plateau.setNbPieceN(plateau.getNbPieceN()-1);
				}
				plateau.getCaseNoire(x-1, y+1).getPiece().setVisible(false);
				plateau.getCaseNoire(x-1, y+1).getPiece().setMaCase(null);
				plateau.getCaseNoire(x-1, y+1).setPiece(null);
			}
		}
		else if(mX==-1&&mY==-1) {
			if(plateau.getCaseNoire(x+1, y+1).getPiece()!=null) {
				if(plateau.getCaseNoire(x+1, y+1).getPiece().getCouleur()=='b') {
					plateau.setNbPieceB(plateau.getNbPieceB()-1);
				}
				else if(plateau.getCaseNoire(x+1, y+1).getPiece().getCouleur()=='n') {
					plateau.setNbPieceN(plateau.getNbPieceN()-1);
				}
				plateau.getCaseNoire(x+1, y+1).getPiece().setVisible(false);
				plateau.getCaseNoire(x+1, y+1).getPiece().setMaCase(null);
				plateau.getCaseNoire(x+1, y+1).setPiece(null);
			}
		}
	}
	public void deplacerPiece() {
		plateau.getPieceSelectionnee().getMaCase().setPiece(null);
		plateau.getPieceSelectionnee().setMaCase(this);
		this.setPiece(plateau.getPieceSelectionnee());
		this.piece.deplacement();
		if((this.piece.getCouleur()=='b'&&y==0)||(this.piece.getCouleur()=='n'&&y==9)) {
			this.piece.setDame(true);
			this.piece.setEstUneDame(true);
		}
		this.piece.selectionner();
		plateau.deselectionnerTout();
		if(manger==true) {
			manger();
			this.piece.setDeplacementUniquement(false);
		}
		if(this.piece.isDeplacementUniquement()==true) {
			plateau.finDeTour();
		}
		else {
			if(this.piece.getCouleur()=='b'&&plateau.uneBlancheSelectionnee()==false) {
				if(this.getX()-1>=0&&this.getY()-1>=0&&plateau.getCaseNoire(this.getX()-1,this.getY()-1).getPiece()!=null&&plateau.getCaseNoire(this.getX()-1,this.getY()-1).getPiece().getCouleur()=='n'&&this.getX()-2>=0&&this.getY()-2>=0&&plateau.getCaseNoire(this.getX()-2,this.getY()-2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else if(this.getX()+1<=9&&this.getY()-1>=0&&plateau.getCaseNoire(this.getX()+1,this.getY()-1).getPiece()!=null&&plateau.getCaseNoire(this.getX()+1,this.getY()-1).getPiece().getCouleur()=='n'&&this.getX()+2<=9&&this.getY()-2>=0&&plateau.getCaseNoire(this.getX()+2,this.getY()-2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else if(this.getX()-1>=0&&this.getY()+1<=9&&plateau.getCaseNoire(this.getX()-1,this.getY()+1).getPiece()!=null&&plateau.getCaseNoire(this.getX()-1,this.getY()+1).getPiece().getCouleur()=='n'&&this.getX()-2>=0&&this.getY()+2<=9&&plateau.getCaseNoire(this.getX()-2,this.getY()+2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else if(this.getX()+1<=9&&this.getY()+1<=9&&plateau.getCaseNoire(this.getX()+1,this.getY()+1).getPiece()!=null&&plateau.getCaseNoire(this.getX()+1,this.getY()+1).getPiece().getCouleur()=='n'&&this.getX()+2<=9&&this.getY()+2<=9&&plateau.getCaseNoire(this.getX()+2,this.getY()+2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else {
					plateau.finDeTour();
				}
			}
			else if(this.piece.getCouleur()=='n'&&plateau.uneNoireSelectionnee()==false) {
				if(this.getX()-1>=0&&this.getY()+1<=9&&plateau.getCaseNoire(this.getX()-1,this.getY()+1).getPiece()!=null&&plateau.getCaseNoire(this.getX()-1,this.getY()+1).getPiece().getCouleur()=='b'&&this.getX()-2>=0&&this.getY()+2<=9&&plateau.getCaseNoire(this.getX()-2,this.getY()+2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else if(this.getX()+1<=9&&this.getY()+1<=9&&plateau.getCaseNoire(this.getX()+1,this.getY()+1).getPiece()!=null&&plateau.getCaseNoire(this.getX()+1,this.getY()+1).getPiece().getCouleur()=='b'&&this.getX()+2<=9&&this.getY()+2<=9&&plateau.getCaseNoire(this.getX()+2,this.getY()+2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else if(this.getX()+1<=9&&this.getY()-1>=0&&plateau.getCaseNoire(this.getX()+1,this.getY()-1).getPiece()!=null&&plateau.getCaseNoire(this.getX()+1,this.getY()-1).getPiece().getCouleur()=='b'&&this.getX()+2<=9&&this.getY()-2>=0&&plateau.getCaseNoire(this.getX()+2,this.getY()-2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else if(this.getX()-1>=0&&this.getY()-1>=0&&plateau.getCaseNoire(this.getX()-1,this.getY()-1).getPiece()!=null&&plateau.getCaseNoire(this.getX()-1,this.getY()-1).getPiece().getCouleur()=='b'&&this.getX()-2>=0&&this.getY()-2>=0&&plateau.getCaseNoire(this.getX()-2,this.getY()-2).getPiece()==null) {
					this.piece.setDame(false);
					this.piece.selectionner();
				}
				else {
					plateau.finDeTour();
				}
			}
		}
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public Plateau getPlateau() {
		return plateau;
	}
	public Piece getPiece() {
		return piece;
	}
	public void setPiece(Piece piece) {
		this.piece = piece;
	}
	public boolean isSelectionnee() {
		return selectionnee;
	}
	public void setmX(int mX) {
		this.mX = mX;
	}
	public void setmY(int mY) {
		this.mY = mY;
	}
	public void setManger(boolean manger) {
		this.manger = manger;
	}
	public boolean isManger() {
		return manger;
	}
}